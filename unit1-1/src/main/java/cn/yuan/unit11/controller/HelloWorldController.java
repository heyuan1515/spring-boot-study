package cn.yuan.unit11.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by heyua on 2017/6/22 0022.
 */
@RestController
public class HelloWorldController {

    @GetMapping(value = "/hello")
    public String index(){
        return "Hello World!";
    }

}
