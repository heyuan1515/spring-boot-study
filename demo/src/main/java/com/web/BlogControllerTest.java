package com.web;

import com.entity.Blog;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Created by H on 2017/6/6.
 */
@RestController
@RequestMapping(value = "/blog/test")
public class BlogControllerTest {

    private static Map<Long,Blog> map = Collections.synchronizedMap(new HashMap<Long, Blog>());

    @ApiOperation(value="获取Blog列表", notes="")
    @GetMapping(value = "/list")
    public List<Blog> getList(){
        List<Blog> list = new ArrayList<>(map.values());
        return list;
    }

    @PostMapping(value = "/insert")
    public String insertBlog(@ModelAttribute Blog blog){
        map.put(blog.getId(),blog);
        return "success";
    }

    @GetMapping(value = "/get/{id}")
    public Blog getBlog(@PathVariable Long id){
        return map.get(id);
    }

    @PutMapping(value = "/update/{id}")
    public String updateBlog(@PathVariable Long id,@ModelAttribute Blog blog){
        Blog b = map.get(id);
        b.setContent(blog.getContent());
        b.setTitle(blog.getTitle());
        map.put(id,b);
        return "success";
    }

    @DeleteMapping(value = "/delete/{id}")
    public String deleteBlog(@PathVariable Long id){
        map.remove(id);
        return "success";
    }
}
