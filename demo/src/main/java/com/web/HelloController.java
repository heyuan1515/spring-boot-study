package com.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by H on 2017/6/5.
 */
@RestController
public class HelloController {

    @Value("${parameter.application.name}")
    private String name;

    @Value("${parameter.application.version}")
    private String version;

    @RequestMapping("/hello")
    public String hello(){
        return "hello world! This is "+name+" in version "+version  ;
    }
}
