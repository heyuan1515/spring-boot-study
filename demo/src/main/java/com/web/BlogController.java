package com.web;

import com.entity.Blog;
import com.service.impl.BlogServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by H on 2017/6/6.
 */
@RestController
@RequestMapping(value = "/blog")
public class BlogController {

    @Autowired
    private BlogServiceImpl service;

    @ApiOperation(value = "获取Blog列表")
    @GetMapping(value = "/list")
    public List<Blog> getList() {
        List<Blog> list = service.findList();
        return list;
    }

    @ApiOperation(value = "根据id获取Blog")
    @GetMapping(value = "/get/{id}")
    public Blog getBlog(@PathVariable Long id) {
        return service.getById(id);
    }

}
