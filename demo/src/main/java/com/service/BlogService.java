package com.service;

import com.dao.BlogDao;
import com.entity.Blog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by H on 2017/6/6.
 */
public interface BlogService {

    List<Blog> findList();

    Blog getById(Long id);

    void insert(Blog blog);

    void updateById(Blog blog);

    void deleteById(Long id);
}
