package com.service.impl;

import com.dao.BlogDao;
import com.entity.Blog;
import com.service.BlogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by H on 2017/6/6.
 */
@Service
@CacheConfig(cacheNames = "blogCache")
public class BlogServiceImpl implements BlogService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlogServiceImpl.class);

    @Autowired
    private BlogDao dao;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 获取列表信息
     *
     * @return
     */
    @Override
    public List<Blog> findList() {
        return dao.findList();
    }

    /**
     * 根据id获取信息
     *
     * @param id
     * @return
     */
    @Override
    @Cacheable(key = "'getById:'+#id")
    public Blog getById(Long id) {
        // 从 DB 中获取blog信息
        Blog blog = dao.getById(id);
        return blog;
    }

    /**
     * 新增一条
     *
     * @param blog
     */
    @Override
    public void insert(Blog blog) {

    }

    /**
     * 根据id更新信息
     *
     * @param blog
     */
    @Override
    public void updateById(Blog blog) {

    }

    /**
     * 根据id删除一条信息
     *
     * @param id
     */
    @Override
    public void deleteById(Long id) {

    }
}
