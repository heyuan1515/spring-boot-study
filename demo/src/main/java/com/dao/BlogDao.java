package com.dao;

import com.entity.Blog;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by H on 2017/6/6.
 */
@Mapper
public interface BlogDao {

    List<Blog> findList();

    Blog getById(@Param("id") Long id);
}
