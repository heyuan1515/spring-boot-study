package com;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DemoApplicationTests {

    @Autowired
    private MockMvc mvc;

    @Test
    public void contextLoads() throws Exception {
        String url = "/hello";
        MvcResult mvcResult = mvc.perform(get(url).accept(MediaType.APPLICATION_JSON)).andReturn();
        mvcResult.getResponse();
    }

    @Test
    public void testBlogController() throws Exception {
        String url = "/blog";
        RequestBuilder request = null;
        MvcResult mvcResult = null;

        // 1、get查一下blog列表，应该为空
        request = get(url + "/list/");
        mvcResult = mvc.perform(request).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());

        // 2、post提交一个blog
        request = post(url + "/insert/")
                .param("id", "1")
                .param("title", "标题1")
                .param("content", "内容1");
        mvcResult = mvc.perform(request).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());

        // 3、get获取blog列表，应该有刚才插入的数据
        request = get(url + "/list/");
        mvcResult = mvc.perform(request).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());

        // 4、put修改id为1的blog
        request = put(url + "/update/1")
                .param("title", "title-one")
                .param("content", "content-one");
        mvcResult = mvc.perform(request).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());

        // 5、get一个id为1的blog
        request = get(url + "/get/1");
        mvcResult = mvc.perform(request).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());

        // 6、del删除id为1的blog
        request = delete(url + "/delete/1");
        mvcResult = mvc.perform(request).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());

        // 7、get查一下blog列表，应该为空
        request = get(url + "/list/");
        mvcResult = mvc.perform(request).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());

    }

}