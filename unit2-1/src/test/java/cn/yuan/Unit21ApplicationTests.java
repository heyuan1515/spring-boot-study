package cn.yuan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class Unit21ApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void contextLoads() throws Exception {
        String baseUrl = "/student";
        RequestBuilder requestBuilder = null;
        MvcResult mvcResult = null;

        // 先查询学生列表
        requestBuilder = get(baseUrl + "/list");
        mvcResult = mockMvc.perform(requestBuilder).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());

        // 新增一条
        requestBuilder = post(baseUrl + "/insert")
                .param("name", "test")
                .param("idCard", "test001")
                .param("age", "1");
        mvcResult = mockMvc.perform(requestBuilder).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());

        // 查询id为1的学生
        requestBuilder = get(baseUrl+"/getById/1");
        mvcResult = mockMvc.perform(requestBuilder).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());

        // 更新id为1的学生
        requestBuilder = post(baseUrl + "/updateById/1")
                .param("name", "test002")
                .param("idCard", "test002")
                .param("age", "1");
        mvcResult = mockMvc.perform(requestBuilder).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());

        // 再查询id为1的学生
        requestBuilder = get(baseUrl+"/getById/1");
        mvcResult = mockMvc.perform(requestBuilder).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());

        // 删除id为1的学生
        requestBuilder = delete(baseUrl+"/deleteById/1");
        mvcResult = mockMvc.perform(requestBuilder).andReturn();
        System.out.printf(mvcResult.getResponse().getContentAsString());
    }

}
