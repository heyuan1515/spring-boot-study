package cn.yuan.controller;

import cn.yuan.entity.Student;
import cn.yuan.service.StudentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by heyua on 2017/6/23 0023.
 */
@RestController
@RequestMapping(value = "/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping(value = "/list")
    @ApiOperation("学生列表")
    public List<Student> findList(){
        return studentService.findList();
    }

    @GetMapping(value = "/getById/{id}")
    @ApiOperation("单个学生信息")
    public Student getById(@PathVariable int id){
        return studentService.getById(id);
    }

    @PostMapping(value = "/insert")
    @ApiOperation("新增学生信息")
    public String insert(Student student){
        studentService.insert(student);
        return "success";
    }

    @PutMapping(value = "/updateById/{id}")
    @ApiOperation("更新学生信息")
    public String updateById(@PathVariable int id , Student student){
        student.setId(id);
        studentService.updateById(student);
        return "success";
    }

    @DeleteMapping(value = "/deleteById/{id}")
    @ApiOperation("删除学生信息")
    public String deleteById(@PathVariable int id ){
        studentService.deleteById(id);
        return "success";
    }
}
