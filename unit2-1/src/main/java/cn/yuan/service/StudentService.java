package cn.yuan.service;

import cn.yuan.dao.StudentDao;
import cn.yuan.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by heyua on 2017/6/23 0023.
 */
@Service
public class StudentService {

    @Autowired
    private StudentDao studentDao;


    public List<Student> findList() {
        return studentDao.findList();
    }

    public Student getById(int id) {
        return studentDao.getById(id);
    }

    public void insert(Student student) {
        studentDao.insert(student);
    }

    public void updateById(Student student) {
        studentDao.updateById(student);
    }

    public void deleteById(int id) {
        studentDao.deleteById(id);
    }
}
