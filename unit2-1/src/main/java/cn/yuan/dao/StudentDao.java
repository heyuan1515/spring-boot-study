package cn.yuan.dao;

import cn.yuan.entity.Student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by heyua on 2017/6/23 0023.
 */
@Mapper
public interface StudentDao {
    List<Student> findList();

    Student getById(@Param("id") int id);

    void insert(Student student);

    void updateById(Student student);

    void deleteById(@Param("id") int id);
}
