package cn.yuan.service;

import cn.yuan.dao.StudentDao;
import cn.yuan.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by heyua on 2017/6/23 0023.
 */
@Service
@CacheConfig(cacheNames = "student")
public class StudentService {

    @Autowired
    private StudentDao studentDao;

    @Cacheable(cacheNames = "findList")
    public List<Student> findList() {
        return studentDao.findList();
    }

    public Student getById(int id) {
        return studentDao.getById(id);
    }

    public void insert(Student student) {
        studentDao.insert(student);
    }

    public void updateById(Student student) {
        studentDao.updateById(student);
    }

    public void deleteById(int id) {
        studentDao.deleteById(id);
    }
}
