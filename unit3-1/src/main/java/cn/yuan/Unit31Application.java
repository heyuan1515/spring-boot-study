package cn.yuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class Unit31Application {

	public static void main(String[] args) {
		SpringApplication.run(Unit31Application.class, args);
	}
}
